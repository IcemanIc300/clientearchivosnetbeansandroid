package com.example.vencar;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintWriter;
import java.net.Socket;

public class MainActivity extends Activity implements OnClickListener {

    Button btnCrearArchivos, btnAbrir, btnManipular, btnConectar;
    TextView txtOriginal, txtResultado;

    private final int PICKER = 1;
    String rutaArchivo, direccionIP;
    int puertoServidor;

    File original;
    int idOperacion;
    int numLineas;
    BufferedReader lector;

    Socket peticionesServidor;
    PrintWriter alServidor;
    DataInputStream delServidor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        btnCrearArchivos = (Button) findViewById(R.id.btnCrearArchivos);
        btnAbrir = (Button) findViewById(R.id.btnAbrir);
        btnManipular = (Button) findViewById(R.id.btnManipular);
        btnConectar = (Button) findViewById(R.id.btnConectar);
        txtOriginal = (TextView) findViewById(R.id.txtOriginal);
        txtResultado = (TextView) findViewById(R.id.txtResultado);

        btnManipular.setEnabled(false);
        btnCrearArchivos.setOnClickListener(this);
        btnAbrir.setOnClickListener(this);
        btnManipular.setOnClickListener(this);
        btnConectar.setOnClickListener(this);
        txtResultado.setOnClickListener(this);

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCrearArchivos:
                desplegarCrearArchivos(null);
                break;
            case R.id.btnAbrir:
                AbrirArchivo();
                break;
            case R.id.btnManipular:
                ManipularArchivo();
                break;
            case R.id.btnConectar:
                EstablecerParametros();
                break;
            case R.id.txtResultado:
                GuardarArchivo();
                break;
        }
    }

    public void desplegarCrearArchivos(View vista) {
        Intent cambiar = new Intent(this, CrearArchivos.class);
        startActivity(cambiar);
    }

    private void AbrirArchivo() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("txt/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Seleccionar Archivo"), PICKER);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "Instale Manejador de Archivos", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            switch (requestCode) {
                case PICKER:
                    if (resultCode == RESULT_OK) {
                        rutaArchivo = data.getData().getPath();
                        original = Environment.getExternalStorageDirectory();
                        lector = new BufferedReader(new FileReader(rutaArchivo));
                        txtOriginal.setText("");
                        numLineas = 0;
                        String cadenaAuxiliar;
                        cadenaAuxiliar = lector.readLine();
                        while (cadenaAuxiliar != null) {
                            txtOriginal.append(cadenaAuxiliar + "\n");
                            cadenaAuxiliar = lector.readLine();
                            numLineas++;
                        }
                        lector.close();
                        btnManipular.setEnabled(true);
                    }
                    if (resultCode == RESULT_CANCELED) {
                        Toast.makeText(this, "Por favor, seleccione un archivo", Toast.LENGTH_LONG).show();
                    }
                    break;
            }

        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    private void ManipularArchivo() {
        final Dialog dialogoOpciones = new Dialog(MainActivity.this);
        dialogoOpciones.setTitle("¿Qué desea hacer?");
        dialogoOpciones.setContentView(R.layout.menu_opciones);
        Button btnCancelar = (Button) dialogoOpciones.findViewById(R.id.btnCancelar);
        
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogoOpciones.dismiss();
            }
        });

        RadioButton rbtnInvertir = (RadioButton) dialogoOpciones.findViewById(R.id.rbtnInvertir);
        rbtnInvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogoOpciones.dismiss();
                Toast.makeText(getApplicationContext(),
                        "Selecciono Invertir", Toast.LENGTH_SHORT).show();
                idOperacion = 0;
                EstablecerConexion();
            }
        });

        RadioButton rbtnInvertirLineas = (RadioButton) dialogoOpciones.findViewById(R.id.rbtnInvertirLineas);
        rbtnInvertirLineas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogoOpciones.dismiss();
                Toast.makeText(getApplicationContext(),
                        "Selecciono Invertir Lineas", Toast.LENGTH_SHORT).show();
                idOperacion = 1;
                EstablecerConexion();
            }
        });

        RadioButton rbtnAmbos = (RadioButton) dialogoOpciones.findViewById(R.id.rbtnAmbos);
        rbtnAmbos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogoOpciones.dismiss();
                Toast.makeText(getApplicationContext(),
                        "Selecciono Invertir Todo", Toast.LENGTH_SHORT).show();
                idOperacion = 2;
                EstablecerConexion();
            }
        });

        dialogoOpciones.show();
    }

    private void EstablecerParametros() {
        final Dialog dialogoDatosServidor = new Dialog(MainActivity.this);
        dialogoDatosServidor.setTitle("Parámetros de Conexión");
        dialogoDatosServidor.setContentView(R.layout.datos_servidor);

        Button btnAceptar = (Button) dialogoDatosServidor.findViewById(R.id.btnAceptar);
        Button btnCancelar = (Button) dialogoDatosServidor.findViewById(R.id.btnCancelar);
        
        final TextView txtIP = (TextView) dialogoDatosServidor.findViewById(R.id.txtIp);
        final TextView txtPuerto = (TextView) dialogoDatosServidor.findViewById(R.id.txtPuerto);
        
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               dialogoDatosServidor.dismiss();
            }
        });

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtIP.getText().length() > 0) {
                    if (txtPuerto.getText().length() > 0) {
                        direccionIP = txtIP.getText().toString();
                        puertoServidor = Integer.parseInt(txtPuerto.getText().toString());
                        txtResultado.setText(puertoServidor + direccionIP);
                        dialogoDatosServidor.dismiss();
                    }
                }
            }
        });
        dialogoDatosServidor.show();
    }

    private void EstablecerConexion() {
        try {
            peticionesServidor = new Socket(direccionIP, puertoServidor);
            alServidor = new PrintWriter(peticionesServidor.getOutputStream(), true);
            delServidor = new DataInputStream(peticionesServidor.getInputStream());
            alServidor.println("Soy un nigga cliente");
            alServidor.println(numLineas);
            alServidor.println(idOperacion);
            lector = new BufferedReader(new FileReader(rutaArchivo));
            String cadenaAuxiliar;
            cadenaAuxiliar = lector.readLine();
            while (cadenaAuxiliar != null) {
                alServidor.println(cadenaAuxiliar);
                cadenaAuxiliar = lector.readLine();
                txtResultado.append("\nEnvie");
            }
            String lineaResultado = "";
            for (int i = 0; i < numLineas; i++) {
                lineaResultado += delServidor.readLine() + "\n";
            }
            lector.close();

            String respuesta = delServidor.readLine();
            txtResultado.setText(lineaResultado);
            CerrarConexion();
        } catch (Exception ex) {
            txtResultado.setText(ex.getMessage());
        }
    }

    private void CerrarConexion() {
        try {
            alServidor.println("Ya me voy");
            alServidor.close();
            delServidor.close();
            peticionesServidor.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    private void GuardarArchivo() {
        final Dialog dialogoGuardar = new Dialog(MainActivity.this);
        dialogoGuardar.setTitle("Guardar Archivo");
        dialogoGuardar.setContentView(R.layout.datos_guardar);

        Button btnGuardar = (Button) dialogoGuardar.findViewById(R.id.btnGuardar);
        Button btnCancelar = (Button) dialogoGuardar.findViewById(R.id.btnCancelar);
        final TextView txtNombreArchivo = (TextView) dialogoGuardar.findViewById(R.id.txtNombreArchivo);
        
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogoGuardar.dismiss();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtResultado.getText().length() > 0) {
                    String nombreArchivo = txtNombreArchivo.getText().toString();
                    String contenidoArchivo = txtResultado.getText().toString();

                    if (nombreArchivo.length() > 0) {
                        if (contenidoArchivo.length() > 0) {
                            FileOutputStream escritor;
                            try {
                                Environment.getExternalStorageState();
                                File directorio = Environment.getExternalStorageDirectory();
                                directorio = new File(directorio.getAbsolutePath() + "/" + nombreArchivo);
                                boolean status = directorio.mkdirs();
                                if (status) {
                                    directorio = new File(directorio + ".txt");
                                    escritor = new FileOutputStream(directorio);
                                    PrintWriter guardar = new PrintWriter(escritor);
                                    guardar.println(contenidoArchivo);
                                    guardar.flush();
                                    guardar.close();
                                    escritor.close();
                                    Toast.makeText(getApplicationContext(), "Se creo el archivo", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getApplication(), "No se pudo crer el archivo", Toast.LENGTH_LONG).show();
                                }
                            } catch (Exception ex) {
                                ex.getMessage();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Ingrese Contenido del Archivo", Toast.LENGTH_LONG).show();
                        }

                        dialogoGuardar.dismiss();
                    } else {
                        dialogoGuardar.dismiss();
                        Toast.makeText(getApplicationContext(), "Archivo Vacio", Toast.LENGTH_LONG).show();
                    }
                }
            }

        });
        dialogoGuardar.show();
    }

}
