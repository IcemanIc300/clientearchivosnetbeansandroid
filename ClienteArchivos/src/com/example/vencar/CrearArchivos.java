package com.example.vencar;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class CrearArchivos extends Activity {

    Button btnCancelar, btnCrear;
    EditText txtNombre, txtContenido;
    File directorio;
    String keyContenido, keyNombre;
    SharedPreferences archivoPreferencias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crear_archivos);

        btnCancelar = (Button) findViewById(R.id.btnCancelar);
        btnCrear = (Button) findViewById(R.id.btnCrearArchivo);
        txtContenido = (EditText) findViewById(R.id.txtContenido);
        txtNombre = (EditText) findViewById(R.id.txtNombreArchivo);

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });

        btnCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crearArchivo();
            }
        });

        recuperarPreferencias();

    }

    public void salir(){
        limpiarCampos();
        Intent regresar = new Intent(this, MainActivity.class);
        startActivity(regresar);
        CrearArchivos.this.finish();
    }

    public void crearArchivo(){
        String nombreArchivo = txtNombre.getText().toString();
        String contenidoArchivo = txtContenido.getText().toString();

        if(nombreArchivo.length() > 0){
            if(contenidoArchivo.length() > 0){
                FileOutputStream escritor;
                try{
                    Environment.getExternalStorageState();
                    directorio = Environment.getExternalStorageDirectory();
                    directorio = new File(directorio.getAbsolutePath() + "/" + nombreArchivo);
                    boolean status = directorio.mkdirs();
                    if(status){
                        directorio = new File(directorio + ".txt");
                        escritor = new FileOutputStream(directorio);
                        PrintWriter guardar = new PrintWriter(escritor);
                        guardar.println(contenidoArchivo);
                        guardar.flush();
                        guardar.close();
                        escritor.close();
                        Toast.makeText(this, "Se creo el archivo", Toast.LENGTH_LONG).show();
                        limpiarCampos();
                    }else{
                        Toast mensaje = Toast.makeText(this,"No se pudo crer el archivo",Toast.LENGTH_LONG);
                        mensaje.show();
                    }
                }catch(Exception ex){
                    ex.getMessage();
                }
            }else{
                Toast mensaje = Toast.makeText(this,"Ingrese Contenido del Archivo",Toast.LENGTH_LONG);
                mensaje.show();
            }
        }else{
            Toast mensaje = Toast.makeText(this,"Ingrese Nombre de Archivo",Toast.LENGTH_LONG);
            mensaje.show();
        }
    }

    public void limpiarCampos(){
        txtContenido.setText(" ");
        txtNombre.setText(" ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        keyContenido = txtContenido.getText().toString();
        keyNombre = txtNombre.getText().toString();
        SharedPreferences.Editor mieditor= archivoPreferencias.edit();
        mieditor.putString("contenido", keyContenido);
        mieditor.putString("nombre", keyNombre);
        mieditor.apply();
    }

    public void recuperarPreferencias(){
        archivoPreferencias = getSharedPreferences("preferencias",0);
        keyContenido = archivoPreferencias.getString("contenido", "");
        keyNombre = archivoPreferencias.getString("nombre", "");
        txtContenido.setText(keyContenido);
        txtNombre.setText(keyNombre);
    }

}
